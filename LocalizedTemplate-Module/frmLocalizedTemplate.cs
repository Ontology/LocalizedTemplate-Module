﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Ontology_Module;
using OntologyClasses.BaseClasses;
using OntologyAppDBConnector;
using OntoMsg_Module;
using System.Runtime.InteropServices;

namespace LocalizedTemplate_Module
{
    public partial class frmLocalizedTemplate : Form
    {

        private clsLocalConfig localConfig;

        

        public frmLocalizedTemplate()
        {
            InitializeComponent();
            localConfig = (clsLocalConfig)LocalConfigManager.GetLocalConfig(((GuidAttribute)Assembly.GetExecutingAssembly().GetCustomAttributes(true).FirstOrDefault(objAttribute => objAttribute is GuidAttribute)).Value);
            if (localConfig == null)
            {
                localConfig = new clsLocalConfig(new Globals());
                LocalConfigManager.AddLocalConfig(localConfig);
            }

            TestAutoCorrection();
        }

        private void TestAutoCorrection()
        {
            frmAutoCorrection objFrmAutoCorrection;
            objFrmAutoCorrection = new frmAutoCorrection(localConfig);
            objFrmAutoCorrection.ValueToSearch = "a";
            objFrmAutoCorrection.selectedCorrectorItem += objFrmAutoCorrection_selectedCorrectorItem;
            objFrmAutoCorrection.Show();
            

        }

        void objFrmAutoCorrection_selectedCorrectorItem(frmAutoCorrection frmAutoCorrection, clsOntologyItem oItem_Selected)
        {
            frmAutoCorrection.Hide();
            MessageBox.Show(this, oItem_Selected.Name, "Value", MessageBoxButtons.OK, MessageBoxIcon.Information);
        }

        

        private void toolStripButton_Close_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
